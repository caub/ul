import { flattenTree, unflattenTree, getNodesBreadthFirst, getSiblingIds, examples, genId, getNode, addNode, deleteNode, moveNode, throttle, Icon, Editable } from './utils';

const { Map } = Immutable;
// start
const examplesMap = Map([['Blank', {}], ...examples])

const styles = {
  '@global': {
    body: {
      margin: 0,
    },
    '.material-icons': {
      userSelect: 'none'
    },
    '.flex-1': {
      flex: 1
    },
    '*': {
      boxSizing: 'border-box',
    },
    'button, [role=button]': {
      background: 'none',
      border: 'none',
      cursor: 'pointer'
    },
    'select': {
      background: 'none',
    },
    'ol,ul': {
      listStyle: 'none',
      padding: 0,
    }
  },
  root: {
    height: '100vh',
  },
  node: {
    minHeight: '2em',
  },
  handle: {
    cursor: 'move',
    top: -12,
    left: -12,
    backgroundColor: 'rgba(0,0,0,.05)',
    borderRadius: '50%',
    height: 24
  },
  action: {
    cursor: 'move'
  },
  placeholder: {
    position: 'absolute',
    pointerEvents: 'none',
    backgroundColor: 'rgba(80,80,80,.8)',
    width: 4,
    borderRadius: 2,
  },
  noEvents: {
    pointerEvents: 'none'
  }
}

// will use a canvas, but we could also render all the graph as svg from a Portal (with some foreignObject for the editable text)

const canvas = document.getElementById('canvas'); // where we draw edges
const ctx = canvas.getContext('2d');

class AppRaw extends React.PureComponent {
  constructor(p) {
    super(p);
    const _dragOverAnswer = (e, id) => { // allow to move between siblings, left or right
      if (id === this.state.draggedId) return this.resetTarget();
      e.preventDefault();
      const { draggedId, nodes } = this.state;
      const r = e.currentTarget.parentNode.getBoundingClientRect();

      const after = e.clientX > r.left + r.width / 2;

      this.setState({
        placeholderStyle: {
          left: after ? r.right - 2 + scrollX : r.left - 2 + scrollX,
          top: r.top + scrollY,
          height: r.height
        },
        targetId: id,
        targetRel: after ? 'after' : 'before'
      });
    };
    const _dragOverQuestion = (e, id) => { // allow to append to children
      if (id === this.state.draggedId) return this.resetTarget();
      e.preventDefault();
      const { draggedId, nodes } = this.state;

      const node = nodes.get(id);

      if (!node.firstChildId) {
        const r = e.currentTarget.parentNode.getBoundingClientRect();
        return this.setState({
          placeholderStyle: {
            left: r.left + r.width / 2 + scrollX,
            top: r.top + scrollY + 135,
            height: r.height
          },
          targetId: id,
          targetRel: 'in'
        })
      }
      const childrenIds = getSiblingIds(nodes, node.firstChildId);
      const el = this.nodeEls.get(childrenIds[childrenIds.length - 1]);
      if (!el) return this.resetTarget();
      const r = el.getBoundingClientRect();
      this.setState({
        placeholderStyle: {
          left: r.right + scrollX,
          top: r.top + scrollY,
          height: r.height
        },
        targetId: childrenIds[childrenIds.length - 1],
        targetRel: 'after',
      })
    };
    this.dragOverAnswer = throttle(_dragOverAnswer, 50);
    this.dragOverQuestion = throttle(_dragOverQuestion, 50);
  }
  state = {
    modelName: '', // model selected by user
    nodes: Map({
      '1': {
        id: '1',
        childrenIds: [],
        question: ''
      }
    }), // all <id, nodes> contained in the model tree, flattened in this map
    rootId: '1', // rootId of this nodes map
    currentId: '1', // node being currently viewed by user
    placeholderStyle: {}
  }
  board = undefined; // main element containing all tree nodes
  nodeEls = Map().asMutable(); // to keep track of node Elements, for drawing edges

  componentDidMount() {
    this.chooseModel('Electric device');
    this.updateCanvasAsync = () => requestAnimationFrame(() => this.updateCanvas());
    this.updateCanvasAsync();

    this.updateKeyDown = e => {
      if (!document.activeElement || !document.activeElement.contentEditable) return;
      this.updateCanvasAsync();
    }
    window.addEventListener('resize', this.updateCanvasAsync);
    window.addEventListener('keydown', this.updateKeyDown);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateCanvasAsync);
    window.removeEventListener('keydown', this.updateKeyDown);
  }
  componentDidUpdate() {
    // update canvas
    this.updateCanvas();
  }

  updateCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    const rect = this.board.getBoundingClientRect();
    canvas.style.top = rect.top + scrollY + 'px';
    canvas.style.left = rect.left + scrollX + 'px';
    canvas.width = rect.width;
    canvas.height = rect.height;
    const ns = this.state.nodes.delete(this.state.rootId); // get all nodes without the root

    ctx.beginPath();
    ctx.strokeStyle = 'rgba(40,40,40,.6)';
    ctx.lineWidth = 2;
    ns.forEach(({ id, parentId }) => {
      const cEl = this.nodeEls.get(id);
      const pEl = this.nodeEls.get(parentId);
      if (cEl && pEl) {
        const cR = cEl.getBoundingClientRect();
        const pR = pEl.getBoundingClientRect();
        ctx.moveTo(pR.left - rect.left + pR.width / 2 + 4, pR.bottom - rect.top - 4);
        ctx.lineTo(cR.left - rect.left + cR.width / 2 + 4, cR.top - rect.top + 4);
      }
    });
    ctx.stroke();
  }

  chooseModel(name) {
    console.log('set model', name);
    const tree = examplesMap.get(name) || {};
    const { rootId, nodes } = flattenTree(tree);
    this.setState({
      modelName: name,
      rootId,
      currentId: rootId,
      nodes,
    })
  }
  downloadModel() {
    const { nodes, rootId } = this.state;
    const tree = unflattenTree({ rootId, nodes })
    alert(JSON.stringify(tree, (k, v) => k === 'id' ? undefined : v, 2))
  }
  resetTarget() {
    this.setState({ placeholderStyle: { height: 0 }, targetId: undefined });
  }

  dragStart(e, id) {
    if (id === this.state.rootId) return; // don't touch to root
    e.dataTransfer.setData('text/custom', 'drag ' + id);
    const nodeEl = e.currentTarget.parentNode.parentNode;
    const { left, top } = nodeEl.getBoundingClientRect();
    nodeEl.style.opacity = .5;
    e.dataTransfer.setDragImage(nodeEl, e.clientX - left, e.clientY - top);
    this.setState({ draggedId: id });
  }
  dragStartAdd(e) {
    const id = genId();
    e.dataTransfer.setData('text/custom', 'drag ' + id);
    const r = e.currentTarget.getBoundingClientRect();
    e.dataTransfer.setDragImage(document.getElementById('addIcon'), e.clientX - r.left, e.clientY - r.top);
    this.setState({ draggedId: id });
  }
  dragEnd() { // cleanup state after drag operation finished
    const { draggedId } = this.state;
    this.setState({ draggedId: undefined, targetId: undefined, targetRel: undefined, placeholderStyle: { height: 0 } });
    const el = this.nodeEls.get(draggedId);
    if (el) {
      el.style.opacity = '';
    }
  }
  drop(e) { // drop action (drop event is annoying for some reason, not always triggered, it seems chrome waits at least 2s to validate a drop, shouldn't use native drag/drop anyway)
    const { draggedId, targetId, targetRel, nodes } = this.state;
    if (!targetId || !draggedId) return; // nothing to do
    e.preventDefault(); // accept drop
    if (targetRel === 'in') {
      this.setState({ nodes: addNode({ nodes, id: draggedId, parentId: targetId }).nodes });
    } else {
      this.setState({ nodes: moveNode({ nodes, id: draggedId, targetId, after: targetRel !== 'before' }) });
    }
    this.dragEnd();
  }
  dropDelete(e) {
    const { draggedId, nodes } = this.state;
    if (!draggedId) return;
    e.preventDefault(); // accept drop
    this.setState({ nodes: deleteNode({ nodes, id: draggedId }) });
    this.dragEnd();
  }

  render() {
    const { modelName, nodes, currentId = rootId, placeholderStyle } = this.state;
    const { classes } = this.props;
    const levels = getNodesBreadthFirst({ rootId: currentId, nodes });

    return (
      <div className={cn('system sans-serif', 'bg-washed-blue', classes.root)}>
        <header className="flex items-center h3 ph2 bg-light-purple white-90">
          <Icon className="ph4 purple">build</Icon>
          <h1 className="normal ttu f3 tracked">Dialog manager</h1>
        </header>
        <main className="pa3">
          <div className="flex items-center">
            <label htmlFor="model-select">Choose a model</label>
            <select id="model-select" className="h2 mh2" value={modelName} onChange={e => this.chooseModel(e.target.value)}>
              {Array.from(examplesMap, ([name, tree]) => (
                <option value={name} key={name}>{name}</option>
              ))}
            </select>
            <Icon component="button" className="black-80 dim" download={modelName + '.json'} onClick={() => this.downloadModel()}>file_download</Icon>

            <Icon
              component="button"
              title="Drag it below to add a new node"
              className={cn('f2', this.state.draggedId ? 'darkred' : 'black-70', 'ml5', 'dim', classes.action)}
              draggable
              onDragStart={e => this.dragStartAdd(e)}
              onDragEnd={e => this.dragEnd(e)}
              onDrop={e => this.dropDelete(e)}
              onDragOver={e => e.preventDefault()}
            >
              {this.state.draggedId ? 'delete_forever' : 'add_circle_outline'}
            </Icon>
          </div>
          <div
            className={cn('flex', 'flex-column', 'items-center', 'mh5', 'mv3', 'pa3', 'br1')}
            ref={el => { this.board = el; }}
            onDragEnd={e => this.dragEnd(e)}
            onDragOver={e => { if (this.state.draggedId) e.preventDefault(); }}
          >
            {levels.map((row, i) => (
              <div key={i} className="flex items-center">
                {row.map(([parentId, children]) => (
                  <div key={'g_' + parentId} className="flex justify-center items-start flex-1">
                    {children.map(node => (
                      <div
                        key={node.id}
                        className={cn('mb4', 'f4', row.length === 1 && 'flex-1')}
                        ref={el => this.nodeEls.set(node.id, el)}
                        onDrop={e => this.drop(e)}
                        onDragLeave={e => e.currentTarget === e.target && this.resetTarget()}
                      >
                        {i > 0 && (
                          <Editable
                            className={cn('ma1', 'pa2', 'bg-lightest-blue', classes.node)}
                            value={node.answer}
                            onChange={val => this.setState({
                              nodes: nodes.set(node.id, { ...node, answer: val })
                            })}
                            onDragOver={e => this.dragOverAnswer(e, node.id)}
                          >
                            <Icon
                              role="button"
                              title="Move this node by dragging it"
                              className={cn('black-50', 'absolute', 'flex', 'items-center', classes.handle, this.state.draggedId && this.state.draggedId !== node.id && classes.noEvents)}
                              draggable
                              onDragStart={e => this.dragStart(e, node.id)}
                            >drag_handle</Icon>
                          </Editable>
                        )}
                        <Editable
                          className={cn('ma1', 'pa2', 'bg-light-yellow', classes.node)}
                          value={node.question}
                          onChange={val => this.setState({
                            nodes: nodes.set(node.id, { ...node, question: val })
                          })}
                          onDragOver={e => this.dragOverQuestion(e, node.id)}
                        />
                      </div>
                    ))}
                  </div>
                ))}
              </div>
            ))}
          </div>
        </main>
        <div className={classes.placeholder} style={placeholderStyle} />
      </div>
    )
  }
}

const App = injectSheet(styles)(AppRaw);

export default App;
