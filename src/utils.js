if (typeof window === 'undefined') {
  global.crypto = {
    getRandomValues: () => require('crypto').randomBytes(6)
  }
  global.React = {
    PureComponent: Object
  }
  global.injectSheet = () => () => { };
  global.Immutable = {
    Map: o => new global.Map(o && (Array.isArray(o) ? o : Object.entries(o)))
  };
}
// start
const { Map } = Immutable;

const _arr = new Uint32Array(1);
export const genId = () => Array.from(crypto.getRandomValues(_arr), x => x.toString(36)).join('.');

export const debounce = (fn, ms = 500) => {
  let timeout;
  return (...a) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      fn(...a);
    }, ms);
  };
}
export const throttle = (fn, ms = 500) => {
  let d = 0;
  return (...a) => {
    let _d = Date.now();
    if (_d - d > ms) {
      fn(...a);
      d = _d;
    }
  }
}

export const examples = [
  [
    'Electric device', {
      question: 'Does it work?',
      children: [{
        answer: 'Yes',
        question: 'Is there an issue?',
        children: [{
          answer: 'Nothing, sorry',
          question: `If you're satisfied, can we close this request?`,
          children: [{
            answer: 'Yes'
          }]
        }]
      }, {
        answer: 'No',
        question: 'Did you try to turn it off and on?',
        children: [{
          answer: 'Yes',
          question: 'Which best define your issue?',
          children: [{
            answer: `The device can't be started`
          }, {
            answer: 'The device stops by itself after a few minutes'
          }, {
            answer: 'The device stopped working after a few usages'
          }]
        }, {
          answer: 'No'
        }, {
          answer: `I don't remember`
        }]
      }]
    }
  ], [
    'Sunglasses', {
      question: 'What kind of problem do you have?',
      children: [{
        answer: 'They were damaged during transport',
        question: 'Did you remove the protection layer?',
        children: [{
          answer: 'No',
        }, {
          answer: 'Yes'
        }]
      }, {
        answer: '...'
      }]
    }
  ]
];

/**
 * flatten tree (and assign `id` to nodes if it's missing)
 * @returns {rootId: String, nodes: Map<String, Node>} // Node: {id, parentId, firstChildId, nextSiblingId, previousSiblingId, answer, question}
 */
export const flattenTree = ({ id: rootId = genId(), question, children }) => {
  let nodes = Map();
  const recurse = ({ children = [], ...node }) => {
    const childrenWithIds = children.map(c => ({ id: c.id || genId(), ...c }));
    nodes = nodes.set(node.id, { ...node, firstChildId: childrenWithIds[0] && childrenWithIds[0].id });
    childrenWithIds.forEach((c, i) => {
      recurse({
        parentId: node.id,
        nextSiblingId: childrenWithIds[i + 1] && childrenWithIds[i + 1].id,
        previousSiblingId: childrenWithIds[i - 1] && childrenWithIds[i - 1].id,
        ...c
      });
    });
  }
  recurse({ id: rootId, question, children });
  return {
    rootId,
    nodes
  }
};

export const unflattenTree = ({ rootId, nodes }) => {
  const getDeepNode = nodeId => {
    const { firstChildId, parentId, nextSiblingId, previousSiblingId, ...node } = nodes.get(nodeId);
    if (!firstChildId) return node;

    return { ...node, children: getSiblingIds(nodes, firstChildId).map(getDeepNode) };
  };

  return getDeepNode(rootId);
}

export const getNodesBreadthFirst = ({ rootId, nodes }) => {
  let groups = [
    [null, [nodes.get(rootId)]]
  ]; // parentId, childrenIds entries
  const levels = [];
  while (groups.length) {
    levels.push(groups);
    const childrenGroups = [].concat(...groups
      .map(([parentId, children]) =>
        children
          .map(node => {
            if (!node.firstChildId) return [];
            return [node.id, getSiblingIds(nodes, node.firstChildId).map(cId => nodes.get(cId))];
          })
          .filter(pair => pair.length)
      )
    );
    groups = childrenGroups;
  }
  return levels;
}

export const getPath = ({ nodes, id }) => {
  const arr = [];
  let { parentId, answer } = nodes.get(id) || {};
  while (parentId) {
    const parentNode = nodes.get(parentId) || {};
    arr.push({ id: parentId, answer, question: parentNode.question });
    answer = parentNode.answer;
    parentId = parentNode.parentId;
  }
  return arr.reverse();
}

/** 
 * @returns Array of Siblings of Node id
 * @param {*} nodes 
 * @param {*} id 
 */
export const getSiblingIds = (nodes, id) => {
  const siblingIds = [];
  let nextId = id;
  while (nodes.has(nextId)) {
    siblingIds.push(nextId);
    nextId = nodes.get(nextId).nextSiblingId;
  }
  return siblingIds;
}

/**
 * @returns Node with additional children property for convenience (might want to add a class and method later)
 * @param {*} nodes 
 * @param {*} id 
 */
export const getNode = (nodes, id) => ({
  ...nodes.get(id),
  children: getSiblingIds(nodes, nodes.get(id) && nodes.get(id).firstChildId).map(cId => nodes.get(cId))
})

/**
 * @returns {*} {nodes, id: String}
 * @param {*} {nodes: Map<id, Node>, parentId: String (,id: String)}
 */
export const addNode = ({ nodes, parentId, id = genId() }) => {
  if (containsNode(nodes, id, parentId)) {
    return { nodes }; // impossible action
  }
  const m = deleteNode({ nodes, id });
  const parentNode = m.get(parentId);
  if (!parentNode.firstChildId) {
    return {
      id,
      nodes: m
        .set(parentId, { ...parentNode, firstChildId: id })
        .set(id, { question: '', answer: '', ...m.get(id), parentId, id })
    };
  }
  const siblingIds = getSiblingIds(ns, parentNode.firstChildId);
  const last = nodes.get(siblingIds[siblingIds.length - 1]);
  return {
    id,
    nodes: m
      .set(last.id, { ...last, nextSiblingId: id })
      .set(id, { question: '', answer: '', ...m.get(id), parentId, id, previousSiblingId: last.id })
  };
}

/**
 * @returns nodes: Map<id, Node> new nodes of nodes
 * @param {*} {nodes, id: String} 
 * @param {*} recursive 
 */
export const deleteNode = ({ nodes, id }, recursive = false) => {
  const node = nodes.get(id);
  if (!node) return nodes;
  let m = nodes;
  if (node.previousSiblingId) {
    m = m.set(node.previousSiblingId, { ...m.get(node.previousSiblingId), nextSiblingId: node.nextSiblingId });
  } else {
    m = m.set(node.parentId, { ...m.get(node.parentId), firstChildId: node.nextSiblingId });
  }
  if (node.nextSiblingId) {
    m = m.set(node.nextSiblingId, { ...m.get(node.nextSiblingId), previousSiblingId: node.previousSiblingId });
  }
  m = m.set(node.id, { ...node, previousSiblingId: undefined, nextSiblingId: undefined });
  if (recursive && node.firstChildId) {
    let cId = node.firstChildId;
    while (cId) {
      let child = m.get(cId);
      cId = child.nextSiblingId;
      m = deleteNode(cId);
    }
  }
  return m;
};

/**
 * @returns nodes: Map<id, Node> newMap
 * @param {*} {nodes, id: nodeId to move, targetId: reference node id, after: Boolean} 
 */
export const moveNode = ({ nodes, id, targetId, after = true }) => {
  if (containsNode(nodes, id, targetId)) {
    return nodes; // impossible action
  }
  // first remove id from the siblings
  let m = deleteNode({ nodes, id });
  const node = nodes.get(id) || { id, answer: '...' }; // if not found in nodes, it's a new node action
  const target = m.get(targetId);
  if (after) {
    if (target.nextSiblingId) {
      m = m.set(target.nextSiblingId, { ...m.get(target.nextSiblingId), previousSiblingId: node.id });
    }
    m = m.set(target.id, { ...target, nextSiblingId: node.id });
    m = m.set(node.id, { ...node, parentId: target.parentId, previousSiblingId: target.id, nextSiblingId: target.nextSiblingId });
  } else {
    if (target.previousSiblingId) {
      m = m.set(target.previousSiblingId, { ...m.get(target.previousSiblingId), nextSiblingId: node.id })
    } else { // handle parent firstChildId
      m = m.set(target.parentId, { ...m.get(target.parentId), firstChildId: node.id });
    }
    m = m.set(target.id, { ...target, previousSiblingId: node.id });
    m = m.set(node.id, { ...node, parentId: target.parentId, previousSiblingId: target.previousSiblingId, nextSiblingId: target.id });
  }
  return m;
}

/**
 * @returns true if node with id id1 contains id2, false otherwise
 * @param {*} nodes 
 * @param {*} id1 
 * @param {*} id2 
 */
export const containsNode = (nodes, id1, id2) => {
  if (id1 === id2) return true;
  const node = nodes.get(id1);
  if (!node || !node.firstChildId) return false;
  let cId = node.firstChildId;
  while (cId) {
    const child = nodes.get(cId);
    if (containsNode(nodes, cId, id2)) return true;
    cId = child.nextSiblingId;
  }
  return false;
}


// React Components:

export const Icon = ({ className, component: Component = 'i', ...p }) => (
  <Component className={cn('material-icons', className)} {...p} />
);

// Component for editing text
const editableStyles = {
  root: {
    '& .material-icons': {
      fontSize: '.8em',
      padding: [2, 6],
    }
  },
  div: {
    minWidth: '1.5em',
    '&:empty::before': {
      content: '"…"',
      opacity: .2
    }
  }
};
class EditableRaw extends React.PureComponent {
  state = { editing: false }
  div = undefined; // ref to root Element
  edit() {
    this.setState({ editing: true }, () => this.div.focus());
  }
  blur() {
    this.props.onChange(this.div.textContent);
    this.setState({ editing: false }, () => this.div.blur());
    // todo handle onChange events from keydown (document.activeElement), paste, drop events
  }
  render() {
    const { classes, value, onChange, onDelete, className, children, ...p } = this.props;
    return (
      <div className={cn('flex', 'relative', 'items-center', classes.root, className)} {...p}>
        <div
          className={classes.div}
          onDoubleClick={() => this.edit()}
          dangerouslySetInnerHTML={{ __html: markdownToHtml(value) }}
          ref={el => { this.div = el; }}
          contentEditable={this.state.editing}
          onBlur={() => this.blur()}
        />
        {this.state.editing ? (
          <Icon key="done" onClick={() => this.blur()} role="button" className="green">done</Icon>
        ) : (
            <Icon key="edit" onClick={() => this.edit()} role="button" className="black-70">edit</Icon>
          )}
        {children}
      </div>
    )
  }
}
// we should use draft.js ideally
const markdownToHtml = (text = '') => text.replace(/<script/g, '&lt;script').replace(/(!)?\[([^\]]+)\]\(([^)]+)\)/g, (_, i, t, u) => i ? `<img src="${u}" title="${t}" onload="event.target.dispatchEvent(new KeyboardEvent('keyup', event))">` : `<a href="${u}">${t}</a>`);

export const Editable = injectSheet(editableStyles)(EditableRaw);
