import { flattenTree, unflattenTree, examples, getNode, addNode, deleteNode, moveNode, getPath, Icon, Editable } from './utils';

const { Map } = Immutable;
// start
const examplesMap = Map([['Blank', {}], ...examples])

const styles = {
  '@global': {
    body: {
      margin: 0,
    },
    '.material-icons': {
      userSelect: 'none'
    }
  },
  root: {
    height: '100vh',
    '& *': {
      boxSizing: 'border-box',
    },
    '& button, [role=button]': {
      background: 'none',
      border: 'none',
      cursor: 'pointer'
    },
    '& select': {
      background: 'none',
    },
    '& ol,ul': {
      listStyle: 'none',
      padding: 0,
    }
  },
  node: {
    minHeight: '15em',
    backgroundColor: 'rgba(204,204,204,.2)'
  },
  question: {
    flex: '1 1 25em'
  },
  children: {
    flex: '4 4 60em'
  },
  child: {
    backgroundColor: 'rgba(204,204,204,.2)'
  },
  handle: {
    cursor: 'move'
  },
  breadcrumb: {
    minHeight: 24,
    '& li + li::before': {
      content: '"–"'
    }
  }
}

class AppRaw extends React.PureComponent {
  state = {
    modelName: '', // model selected by user
    nodes: Map({
      1: {
        id: '1',
        childrenIds: [],
        question: ''
      }
    }), // all <id, nodes> contained in the model tree, flattened in this map
    rootId: '1', // rootId of this nodes map
    currentId: '1', // node being currently viewed by user
  }

  componentDidMount() {
    this.chooseModel('Electric device')
  }

  chooseModel(name) {
    console.log('set model', name);
    const tree = examplesMap.get(name) || {};
    const { rootId, nodes } = flattenTree(tree);
    this.setState({
      modelName: name,
      rootId,
      currentId: rootId,
      nodes,
    })
  }
  downloadModel() {
    const { nodes, rootId } = this.state;
    const tree = unflattenTree({ rootId, nodes })
    alert(JSON.stringify(tree, (k, v) => k === 'id' ? undefined : v, 2))
  }
  dragStart(e, id) {
    e.dataTransfer.setData('text/custom', 'drag ' + id);
    const { left, top } = e.currentTarget.parentNode.getBoundingClientRect();
    e.dataTransfer.setDragImage(e.currentTarget.parentNode, e.clientX - left, e.clientY - top);
    this.setState({ dragged: id });
  }
  dragEnd(e) {
    this.setState({ dragged: undefined });
  }
  dragOver(e, id) {
    if (id === this.state.dragged) return;
    e.preventDefault();
    const { dragged, nodes } = this.state;
    const r = e.currentTarget.getBoundingClientRect();
    this.setState({ nodes: moveNode({ nodes, id: dragged, targetId: id, after: e.clientY > r.top + r.height / 2 }) });
  }

  render() {
    const { modelName, nodes, currentId = rootId } = this.state;
    const node = getNode(nodes, currentId);
    const { classes } = this.props;
    const path = getPath({ nodes, id: currentId });
    return (
      <div className={cn('system sans-serif', 'bg-washed-blue', classes.root)}>
        <header className="flex items-center h3 ph2 bg-light-purple white-90">
          <Icon className="ph4 purple">build</Icon>
          <h1 className="normal ttu f3 tracked">Dialog manager</h1>
        </header>
        <main className="pa3">
          <div className="flex items-center">
            <label htmlFor="model-select">Choose a model</label>
            <select id="model-select" className="h2 mh2" value={modelName} onChange={e => this.chooseModel(e.target.value)}>
              {Array.from(examplesMap, ([name, tree]) => (
                <option value={name} key={name}>{name}</option>
              ))}
            </select>
            <Icon component="button" className="black-80" download={modelName + '.json'} onClick={() => this.downloadModel()}>file_download</Icon>
          </div>
          <ul className={cn('flex', 'items-center', 'mt4', 'mh5', classes.breadcrumb)}>
            {path.map(({ id, answer, question }) => (
              <li key={id} className="mh2" role="button" onClick={() => this.setState({ currentId: id })}>
                <span className="pa2 black-60">{question}</span>
                <span className="pa2 black">{answer}</span>
              </li>
            ))}
          </ul>
          <div className={cn('flex', 'items-center', 'mh5', 'mv3', 'pa3', 'br1', classes.node)}>
            {node.parentId && (
              <Icon
                component="button"
                className="f2 black-90 pv5 absolute left-1 dim"
                title="Back to parent node"
                onClick={() => this.setState({ currentId: node.parentId })}
              >navigate_before</Icon>
            )}
            <div className={cn('flex', 'flex-column', classes.question)}>
              <label htmlFor="question" className="ma1 black-80">Question</label>
              <div className="f3 ma1">
                <Editable key={node.id} value={node.question} onChange={val => this.setState({ nodes: nodes.set(currentId, { ...node, question: val }) })} />
              </div>
            </div>
            <div className={cn('flex', 'flex-column', 'relative', 'ml4', classes.children)}>
              <ol onDragEnd={e => this.dragEnd(e)}>
                {node.children.map(child => (
                  <li
                    key={child.id}
                    className={cn('flex', 'items-center', 'mv1', 'ph2', classes.child)}
                    onDragOver={e => this.dragOver(e, child.id)}
                  >
                    <Icon role="button" className={cn('black-50', classes.handle)} draggable onDragStart={e => this.dragStart(e, child.id)}>drag_handle</Icon>
                    <Editable
                      value={child.answer}
                      onChange={val => this.setState({
                        nodes: val ?
                          nodes.set(child.id, { ...child, answer: val }) :
                          deleteNode({ nodes, id: child.id })
                      })}
                      className="flex-auto pv3 ph1"
                    />
                    <Icon
                      component="button"
                      onClick={() => alert(`path: \n${JSON.stringify(getPath({ nodes, id: child.id }), ['question', 'answer'], 2)}`)}
                      className="black-60 pa3 dim"
                      title="Print conversation to support (email or online)"
                    >chat</Icon>
                    {nodes.get(child.id).firstChildId ? (
                      <Icon
                        component="button"
                        onClick={() => this.setState({ currentId: child.id })}
                        className="pa3 dim"
                        title="Browse node children"
                      >navigate_next</Icon>
                    ) : (
                        <Icon
                          component="button"
                          onClick={() => this.setState({ currentId: child.id, nodes: addNode({ nodes, parentId: child.id }).nodes })}
                          className="pa3 black-70 mr1 f4 dim"
                          title="Add node children"
                        >add</Icon>
                      )}
                  </li>
                ))}
              </ol>
              <Icon
                component="button"
                className="f3 black-70 ml1 absolute left-0 bottom--1"
                title="Add an answer"
                onClick={() => this.setState({ nodes: addNode({ nodes, parentId: node.id }).nodes })}
              >playlist_add</Icon>
            </div>
          </div>
        </main>
      </div>
    )
  }
}

const App = injectSheet(styles)(AppRaw);

export default App;
