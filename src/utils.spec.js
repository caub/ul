
import { genId, examples, flattenTree, unflattenTree, getNodesBreadthFirst, deleteNode, addNode, moveNode } from './utils';

// const tree = examples[0][1];
const tree = {
  id: '1',
  question: 'What kind of problem do you have?',
  children: [{
    id: '2',
    answer: 'They were damaged during transport',
    question: 'Did you remove the protection layer?',
    children: [{
      id: '3',
      answer: 'No',
    }, {
      id: '4',
      answer: 'Yes'
    }]
  }, {
    id: '5',
    answer: '...'
  }]
}

let { rootId, nodes } = flattenTree(tree);

// nodes = deleteNode({ nodes, id: '2' });

nodes = addNode({ nodes, parentId: '2', id: '8' }).nodes;
nodes = addNode({ nodes, parentId: '2', id: '13' }).nodes;

console.log(getNodesBreadthFirst({ rootId, nodes }).map(row => row.map(([pId, children]) => [pId, children.map(x => x.id).join('|')])));

nodes = moveNode({ nodes, id: '13', targetId: '5', after: false });

// console.log(nodes)
// console.log(JSON.stringify(unflattenTree({ rootId, nodes }), null, 2));

console.log(getNodesBreadthFirst({ rootId, nodes }).map(row => row.map(([pId, children]) => [pId, children.map(x => x.id).join('|')])));

nodes = moveNode({ nodes, id: '13', targetId: '8', after: false });

console.log(getNodesBreadthFirst({ rootId, nodes }).map(row => row.map(([pId, children]) => [pId, children.map(x => x.id).join('|')])));
