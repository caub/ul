const fs = require('fs');

// requirement is to export to jsfiddle, so process a little the sources here

const extract = filename => {
  const [first, data = first] = fs.readFileSync(filename).toString().split(/^\/\/ start/m, 2);
  return data
    .replace(/^export default .*$/gm, '')
    .replace(/^export (?!default)/gm, '')
    .replace(/<>/g, '<React.Fragment>') // no babel7 support in fiddle
    .replace(/<\/>/g, '</React.Fragment>') // no babel7 support in fiddle
}

const content = `// globals: React, ReactDOM, reactJss, Immutable, classNames
window.injectSheet = reactJss.default;
window.cn = window.classNames;

${extract('src/utils.js')}
${extract('src/AppDeep.js')}
${extract('src/index.js')}`;

fs.writeFile('._bundle.js', content, console.log)